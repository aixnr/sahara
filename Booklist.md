# Booklist

## Online and Open Source Books & Lectures

| Topic | Author(s) | Title
| ----- | --------- | :----
| Dataviz | Kieran Healy | [Data Visualization: A practical introduction][online_1]
| Data analysis | Jenny Bryan | [STAT 545: Data wrangling, exploration, and analysis with R][online_2]
| Data Science | Rafael Irizarry | [Data Analysis and Prediction Algorithms with R][online_3]
| Computer Science | -- | [Teach Yourself Computer Science][online_4]
| Python | Swaroop C H | [A Byte of Python][online_5]
| Data Science | Jake VanderPlas | [Python Data Science Handbook][online_6]
| Cell Biology| Ron Milo & Rob Phillips | [Cell Biology By The Numbers][online_7]
| Economics | Gollier et al | [CORE - Economics for a changing world][online_8]
| Dataviz | Claus O. Wilke | [Fundamentals of Data Visualization][online_9]
| R | Hadley Wickham | [Advanced R][online_10]
| Data Science | Garrett Grolemund & Hadley Wickham | [R for Data Science][online_11] 
| Data Science | Jerone Janssens | [Data Science at the Command Line][online_12]
| Javascript | Flavio Copes | [The Javascript Beginner's Handbook (2020 Edition)][online_13]
| Polyglot | -- | [Learn X in Y minutes][online_14]
| Polyglot | -- | [The Missing Semester of Your CS Education][online_15]
| Javascript | -- | [The Modern Javascript Tutorial][online_16]
| Rust | Steve Donovan | [A Gentle Introdution to Rust][online_17]
| Bioinformatics | Biostars | [The Biostar Handbbook: A bioinformatics e-book for beginners][online_18]
| Python HPC | Future Learn | [Python in High Performance Computing][online_19]
| SICP | Sara Bander | [Structure and Interpretation of Computer Programs, HTML eBook][online_20]
| SICP | MIT | [MIT 6.001 Structure & Interpretation, 1986][online_21]
| RegEx | Shreyas Minocha | [Regular Expressions for Regular Folk][online_22]
| API | Craig Dennis | [APIs for Beginners - How to use an API][online_23] (freeCodeCamp)
| Data Structures | Steven (NPE) | [Data Structures - Computer Science Course for Beginners](https://www.youtube.com/watch?v=zg9ih6SVACc)
| Computer Networking | Brian Ferrill | [Computer Networking Course - Network Engineering](https://www.youtube.com/watch?v=qiQR5rTSshw)
| JavaScript | Marijn Haverbeke | [Eloquent JavaScript](https://eloquentjavascript.net/)
| CSS | Google Dev | [Learn CSS](https://web.dev/learn/css/)
| Statistics | Susan Holmes and Wolfgang Huber | [Modern Statistics for Modern Biology](https://www.huber.embl.de/msmb/)
| Statistics | M. Cetinkaya-Rundel & J. Hardin | [Open Intro to Modern Statistics](https://www.openintro.org/book/ims/)

[online_1]:https://socviz.co/index.html
[online_2]:https://stat545.com/index.html
[online_3]:https://rafalab.github.io/dsbook/
[online_4]:https://teachyourselfcs.com/
[online_5]:https://python.swaroopch.com/
[online_6]:https://jakevdp.github.io/PythonDataScienceHandbook/
[online_7]:http://book.bionumbers.org/
[online_8]:https://www.core-econ.org/
[online_9]:https://serialmentor.com/dataviz/
[online_10]:http://adv-r.had.co.nz/
[online_11]:https://r4ds.had.co.nz/
[online_12]:https://www.datascienceatthecommandline.com/
[online_13]:https://www.freecodecamp.org/news/the-complete-javascript-handbook-f26b2c71719c/
[online_14]:https://learnxinyminutes.com/
[online_15]:https://missing.csail.mit.edu/
[online_16]:https://javascript.info/
[online_17]:https://stevedonovan.github.io/rust-gentle-intro/
[online_18]:https://www.biostars.org/p/225812/
[online_19]:https://www.futurelearn.com/courses/python-in-hpc/2
[online_20]:https://github.com/sarabander/sicp
[online_21]:https://www.youtube.com/watch?v=2Op3QLzMgSY&list=PLE18841CABEA24090
[online_22]:https://github.com/shreyasminocha/regex-for-regular-folk
[online_23]:https://www.youtube.com/watch?v=GZvSYJDk-us

## Free PDF Books

| Publisher | Author(s) | Current Ed. | Title
| --------- | --------- | ----------- | :----
| Springer | James et al | 2nd | [An Introduction to Statistical Learning](https://www.statlearning.com/)
| Springer | Hastie et al | 2nd | [The Elements of Statistical Learning](https://web.stanford.edu/~hastie/ElemStatLearn/)

## Dead Tree Books

| Publisher | Author(s) | Current Ed. | Title | Price
| --------- | --------- | ----------- | :---- | -----
| Addison-Wesley | Nemeth et al | 5th | [UNIX and Linux System Administration Handbook][bk_1] | $26.68
| MIT | Abelson et al | 2nd | [Structure and Interpretation of Computer Programs][bk_2] | $54.50
| Microsoft | McConnell | 2nd | [Code Complete: A Practical Handbook of Software Construction][bk_3] | $28.99
| MIT | Nisan & Schocken | 1st | [The Elements of Computing Systems][bk_4] | $25.23
| Prentice Hall | Martin | 1st | [Clean Code: A Handbook of Agile Software Craftsmanship][bk_5] | $26.99
| No Starch Press | Matthes | 2nd | [Python Crash Course: A Hands-On, Project-Based Introduction to Programming][bk_6] | $22.99
| O'Reilly | Downey | 2nd | [Think Python: How to Think Like a Computer Scientist][bk_7] | $33.91
| CSHL Press | Royle | -- | [The Digital Cell: Cell Biology as a Data Science][bk_8] | $48.80
| Technics Publications | Kent & Hoberman | 3rd | [Data and Reality: A Timeless Perspective on Perceiving and Managing Information in Our Imprecise World][bk_9] | $32.10
| Yaknyam Press | Ousterhout | 1st | [A Philosophy of Software Design][bk_10] | $18.95
| O'Reilly | Petrov | 1st | [Database Internals: A Deep Dive into How Distributed Data Systems Work][bk_11] | $38.99
| O'Reilly | Gorelick & Ozsvald | 2nd | [High Performance Python][bk_12] | $58.05

[bk_1]:https://www.amazon.com/UNIX-Linux-System-Administration-Handbook/dp/0134277554
[bk_2]:https://www.amazon.com/Structure-Interpretation-Computer-Programs-Engineering/dp/0262510871
[bk_3]:https://www.amazon.com/Code-Complete-Practical-Handbook-Construction/dp/0735619670
[bk_4]:https://www.amazon.com/Elements-Computing-Systems-Building-Principles/dp/0262640686
[bk_5]:https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship-ebook/dp/B001GSTOAM
[bk_6]:https://www.amazon.com/Python-Crash-Course-Hands-Project-Based/dp/1593279280/
[bk_7]:https://www.amazon.com/dp/1491939362/
[bk_8]:https://www.cshlpress.com/default.tpl?action=full&cart=1577312654808211385&--eqskudatarq=1282
[bk_9]:https://www.amazon.com/Data-Reality-Perspective-Perceiving-Information/dp/1935504215
[bk_10]:https://www.amazon.com/Philosophy-Software-Design-John-Ousterhout/dp/1732102201
[bk_11]:https://www.amazon.com/gp/product/1492040347/
[bk_12]:http://shop.oreilly.com/product/0636920268505.do
