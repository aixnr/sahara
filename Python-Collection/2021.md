# Python Collection 2021

All collected Python bookmarks or from newsletter for year 2021.

| Date Posted | Site | Author(s) | Title
| ----------- | ---- | :-------- | :----
| 26 Aug 2020 | Real Python | Dan Bader | [Common Python Data Structures (Guide)](https://realpython.com/python-data-structures/)
| 02 Sep 2020 | Real Python | Ray Johns | [PyTorch vs TensorFlow for Your Python Deep Learning Project](https://realpython.com/pytorch-vs-tensorflow/)
| 04 Sep 2020 | Test Driven | Caleb Pollman | [Building a Concurrent Web Scraper with Python and Selenium](https://testdriven.io/blog/building-a-concurrent-web-scraper-with-python-and-selenium/)
| 30 Sep 2020 | Real Python | Leodanis Pozo Ramos | [Python's map(): Processing Iterables Without a Loop](https://realpython.com/python-map-function/)
| 18 Nov 2020 | Real Python | Bryan Weber | [Python enumerate(): Simplify Looping With Counters](https://realpython.com/python-enumerate/)
| 18 Nov 2020 | -- | Itamar Turner-Trauring | [Reproducible and upgradable Conda environments: dependency management with conda-lock](https://pythonspeed.com/articles/conda-dependency-management/)
| 08 Dec 2020 | -- | Haki Benita | [Exhaustiveness Checking with Mypy](https://hakibenita.com/python-mypy-exhaustive-checking)
| 16 Dec 2020 | Real Python | Moshe Zadka | [The pass Statement: How to Do Nothing in Python](https://realpython.com/python-pass/)
| 28 Dec 2020 | Real Python | Chaitanya Baweja | [Python and MySQL Database: A Practical Introduction](https://realpython.com/python-mysql/)
| 06 Jan 2021 | Real Python | Dylan Castillo | [Develop Data Visualization Interfaces in Python With Dash](https://realpython.com/python-dash/)
| 11 Jan 2021 | Real Python | Ryan Palo | [NumPy Tutorial: Your First Steps Into Data Science in Python](https://realpython.com/numpy-tutorial/#hello-numpy-curving-test-grades-tutorial)
| 11 Feb 2021 | -- | Martin | [Why does my memory usage explode when concatenating dataframes?](https://drawingfromdata.com/pandas/concat/memory/exploding-memory-usage-with-concat-and-categories.html)
| 03 Feb 2021 | Real Python | Leodanis Pozo Ramos | [Qt Designer and Python: Build Your GUI Applications Faster](https://realpython.com/qt-designer-python/)
| 14 Feb 2021 | Statch | wulf | [Speeding up Python code with Nim](https://medium.com/statch/speeding-up-python-code-with-nim-ec205a8a5d9c)
| 11 Oct 2018 | -- | Trey Hunner | [Asterisks in Python: what they are and how to use them](https://treyhunner.com/2018/10/asterisks-in-python-what-they-are-and-how-to-use-them/)
| 22 Feb 2021 | Real Python | Pedro Pregueiro | [Python & APIs: A Winning Combo for Reading Public Data](https://realpython.com/python-api/)
| 28 Jun 2021 | -- | Functools | Martin Heinz | [Functools - The Power of Higher-Order Functions in Python](https://martinheinz.dev/blog/52)
| 19 Sep 2021 | -- | Pip, Poetry | -- | [Improving Python Dependency Management With pipx and Poetry](https://cedaei.com/posts/python-poetry-pipx/)
