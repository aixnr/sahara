# Online Courses

List of content:

- [Course List](#course-list)
- [Course Progress](#course-progress)
- [freeCodeCamp Specials](#freecodecamp-specials)

## Course List

| Platform | Topic | Lecturers | Status | Title
| -------- | ----- | :-------- | ------ | :----
| edX      | Statistics | Hastie & Tibshirani | Listed | [Statistical Learning](https://www.edx.org/course/statistical-learning)
| edX      | High-throughput | Irizarry & Love | Listed | [Statistical Inference and Modeling for High-throughput Experiments](https://www.edx.org/course/statistical-inference-and-modeling-for-high-throug)
| edX      | Bioconductor | Irizarry & Love | Listed | [Advanced Bioconductor](https://www.edx.org/course/advanced-bioconductor)
| edX      | ML | Barzilay, Jaakkola, & Chu | Listed | [Machine Learning with Python: from Linear Models to Deep Learning](https://www.edx.org/course/machine-learning-with-python-from-linear-models-to)
| edX      | Web, Python, JS | Malan & Yu | Listed | [CS50's Web Programming with Python and JavaScript](https://www.edx.org/course/cs50s-web-programming-with-python-and-javascript)
| edX      | CS Intro | Malan, Lloyd, & Yu | Listed | [CS50's Introduction to Computer Science](https://www.edx.org/course/cs50s-introduction-to-computer-science)
| YouTube  | Landing page in HTML, SCSS, JS | Jessica Chan | Listed | [How to Make a Landing Page using HTML, SCSS, and JavaScript - Full Course](https://www.youtube.com/watch?v=aoQ6S1a32j8)
| YouTube  | Data science, Streamlit | Chanin Nantasenamat | Listed | [Build 12 Data Science Apps with Python and Streamlit - Full Course](https://www.youtube.com/watch?v=JwSS70SZdyM)
| Kaggle   | Python, Pandas | Aleksey Bilogur | Listed | [Pandas](https://www.kaggle.com/learn/pandas)
| Kaggle   | Python, Dataviz | Alexis Cook | Listed | [Data Visualization](https://www.kaggle.com/learn/data-visualization)
| Kaggle   | Python, ML, Intro | Dan Becker | Listed | [Intro to Machine Learning](https://www.kaggle.com/learn/intro-to-machine-learning)
| Kaggle   | Python, ML, Intermediate | Alexis Cook | Listed | [Intermediate Machine Learning](https://www.kaggle.com/learn/intermediate-machine-learning)
| YouTube  | Node, Beginner | Mosh Hamedani | Listed | [Node.js Tutorial for Beginners: Learn Node in 1](https://www.youtube.com/watch?v=TlB_eWDSMt4)
| YouTube  | HTML5, CSS3, Flexbox | Brad Traversy | Listed | [Build a Responsive Website with HTML, CSS Grid, Flexbox & More](https://www.youtube.com/watch?v=p0bGHP-PXD4)
| YouTube  | 2021, Guide | Brad Traversy | Listed | [Web Development In 2021 - A Practical Guide](https://www.youtube.com/watch?v=VfGW0Qiy2I0)
| YouTube  | Flask, Series, Marketplace | JSC | Listed | [Flask Full Series - Web Application Development with Python](https://www.youtube.com/watch?v=p068JokuThU&list=PLOkVupluCIjuPtTkhO6jmA76uQR994Wvi)
| YouTube  | Rust, Microsoft | Microsoft Developers | Listed | [Beginner's Series to: Rust](https://www.youtube.com/playlist?list=PLlrxD0HtieHjbTjrchBwOVks_sr8EVW1x)
| TalkPython | Ordering, Twilio, SendGrid | Michael Kennedy | Listed | [Python-powered chat apps with Twilio and SendGrid Course](https://training.talkpython.fm/courses/python-powered-chat-apps-with-twilio-sendgrid-and-flask)

## Course Progress

| Medium | Tags | Presenter | Duration | Current | Title
| ------ | ----- | :-------- | -------- | ------- | :---
| Udemy | Bootcamp, Python | Jose Portilla | 155 | 46 | [Complete Python Bootcamp from Zero to Hero](https://www.udemy.com/course/complete-python-bootcamp/)
| YouTube | JS, Crash course | Traversy Media | 1 h 40 m | Revised | [JavaScript Crash Course For Beginners](https://www.youtube.com/watch?v=hdI2bqOjy3c)
| YouTube | JS, Beginner | Mosh Hamedani | 48 m | Revised | [JavaScript Tutorial for Beginners: Learn JavaScript in 1 Hour [2020]](https://www.youtube.com/watch?v=W6NZfCO5SIk)
| YouTube | HTML, CSS | Jordan Hudgens | 1 h 38 m | nil | [HTML5 & CSS Development: Learn How to Build a Professional Website](https://www.youtube.com/watch?v=5bMdjkfvONE)
| YouTube | Python, OOP | Tim Ruscica | 53 | 28 | [Python Object Oriented Programming (OOP) - For Beginners](https://www.youtube.com/watch?v=JeznW_7DlB0)
| YouTube | Typescript | Alexander Kochergin | 1 h 34 m | nil | [Learn TypeScript - Full Course for Beginners](https://www.youtube.com/watch?v=gp5H0Vw39yw)

### Single or Simple Tutorial

Sometimes, I would spend time watching short & simple tutorials (e.g. getting started tutorial for certain frameworks). Here is the table for that.

| Medium  | Channel | Tags | Duration | Title
| ------- | ------- | :--- | -------- | :----
| YouTube | Pretty Printed | FastAPI, JSON, Backend | 25 m | [Intro to FastAPI - The Best Way to Create APIs in Python?](https://www.youtube.com/watch?v=kCggyi_7pHg)
| YouTube | Traversy Media | Flask, JSON, SQL Alchemy, Marshmallow | 35 m | [REST API With Flask & SQL Alchemy](https://www.youtube.com/watch?v=PTZiDnuC86g)
| YouTube | freeCodeCamp | Git, Branch | 32 m | [Git Branches Tutorial](https://www.youtube.com/watch?v=e2IbNHi4uCI)

## freeCodeCamp Specials

[freeCodeCamp](https://www.freecodecamp.org/) has a lot of tutorials and some of them are like 5 -- 10 hours-long.

| Area | Presenter | Duration | Title | Status
| ---- | --------- | -------- | :---- | ------
| Python, Bioinformatics | Chanin Nantasenamat | 1 h 42 m | [Drug Discovery Using Machine Learning and Data Analysis](https://www.youtube.com/watch?v=jBlTQjcKuaY) | Listed
| DevOps | Colin Chartier | 2 h 18 m | [DevOps Engineering Course for Beginners](https://www.youtube.com/watch?v=j5Zsa_eOXeY) | Listed
| VueJS | Gwendolyn Faraday | 3 h 39 m | [VueJS Course for Beginners 2021](https://www.youtube.com/watch?v=FXpIoQ_rT_c) | Listed
