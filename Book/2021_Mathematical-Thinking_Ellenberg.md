# How Not To Be Wrong: The Power of Mathematical Thinking

by Jordan Ellenberg, total content pages of 437 (paperback, ISBN 978-0-14-321753-6). There are 18 chapters, broken into 5 parts. Tracking started on 09 Sep 2021 (Sunday), started reading long time ago.

| Part | Ch. | Title      | Range   | Tags
| ---- | --- | :--------- | ------- | :---
| 1    | 1   | Sweden     | 21-30   | ACA, Daniel Mitchell, Cato Institute, Nonlinear thinking, Laffer curve, Republican, Taxation, 1974, Tax rate, 70%, Greg Mankiw, Milton Friedman, 
| 1    | 2   | Curved     | 31-49   | 

## Extra Notes

- **When Am I Going to Use This**, the section before Part I Chapter I. Ellenberg started with the story about Abraham Wald (Statistical Research Group, SRG) and the missing bullet holes during the World War II. Also included the story of John von Neumann, but not as involved as Abraham Wald.
