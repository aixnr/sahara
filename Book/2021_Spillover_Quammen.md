# Spillover

by David Quammen, total content pages of 520 (paperback, ISBN 978-0-393-34661-9). There are 9 chapters, with sub-chapters for each chapter. Read started on 23 Jan 2021 (Saturday).

| Chptr | Title      | Sub | Range   | Tags
| ----- | :--------- | --- | ------- | :---
| 1     | Pale Horse | 1   | 13-14   | Hendra, Brisbane, 1994, Zoonosis, Darwinian
| 1     | Pale Horse | 2   | 14-20   | Drama Series, Vic Rail, Peter Reid, Celestial Charm, AHS, Ray Unwin, Hantavirus, American Southwest, 1993, AAHL, EMV, Bats
| 1     | Pale Horse | 3   | 20-24   | Many zoonotic viruses, Smallpox, Polio, WHO, 1980, Salk, Sabin, Monkeypox, Darwinian, Reservoir host, SFV, Locomotion
| 1     | Pale Horse | 4   | 25-34   | Isolating virus, Virologists, Hume Fields, Serosurveillance, Rodents, Mark Preston, 1995, Meningitis, Hendra, 1996, Bats, Flying foxes, 15% & 47%, Linda Selvey, 128 negatives
| 1     | Pale Horse | 5   | 35-38   | FMD, Morbidity, Terminal condition, $27 billion, Pigs, Amplifier host, 30 times, 1788, Captain Arthur Phillip, 9 horses, Convict colony, New South Wales
| 1     | Pale Horse | 6   | 38-45   | Machupo virus, Bolivian, 1959, 40% fatality, Karl Johnson, 3 elements, Disintegration, Cataclysmic rate, Virosphere, William McNeill, HIV-1 group M, 30 million human, Sangha, Cameroon, Emerging, *Legionella pneumophila*, 58%, 300 events, 60.3%, EIDs, Kate Jones
| 1     | Pale Horse | 7   | 45-49   | Cairns, Unnamed vet, Doctor, Patient, 2004, Horse, Brownie, Protocols, Exonerated, Postmortem, Heart, Lung, Bats, Betadine shower, 9-10 days, Antibodies, Hendra
| 2     | 13 Gorillas | 8  | 53-60   | Central Africa, Mayibout 2, 1996, 18 people, Chimpanzee, 7 hours, 68%, Fatality, Eric Leroy, CIRMF, Franceville, Mike Fay, Biologist, 2000 miles survey, Gabon, Tomo Nishihara, Thony M'both, Gorilla pile, 4171 gorillas, Zero gorilla
| 2     | 13 Gorillas | 9  | 60-63   | Ebola, 1976, 4 lineages, Gabon, 60%, Libreville, Forest disruption, Dead apes, Science paper, 5000 gorillas
| 2     | 13 Gorillas | 10 | 64-68   | Billy Karesh, WCS, EcoHealth Alliance, Blood sampling, 2002 die-off, No gorilla, Trish Reed, Anthroponosis
| 2     | 13 Gorillas | 11 | 69-75   | 1976, Two outbreaks, 300 miles, Zaire, Kinshasa, Karl Johnson, International Comission, Special Pathogen Branch, Clean animals, WHO, Monkeypox program, 117 species, 1977, 1979, Small outbreaks, Kikwit, 245 died, USAMRIID, 3066 blood, BSL-4, 1999, 23 years
| 2     | 13 Gorillas | 12 | 76-80   | Trish Reed, Mapping, Distinct lineages, Reston VA, The Hot Zone, Hazelton Research, Philippines, Reston virus, 0 death, Christophe Boesch, Tai Forest Virus
| 2     | 13 Gorillas | 13 | 80-83   | Zaire, Broadly distributed, Reston, 49 NHPs, Uganda, Reservoir mysteries, 224 people, 53%, Virulence
| 2     | 13 Gorillas | 14 | 84-87   | 5th, Ebolavirus, Bundibugyo, 20 people, Jonathan Towner, New, Epidemic, Panic, 5 questions
| 2     | 13 Gorillas | 15 | 87-91   | Malevolent spirits, Mekouka, Barry Hewlett, Anthropologist, Culture, Acholi, Sorcery
| 2     | 13 Gorillas | 16 | 91-96   | Richard Preston, The Hot Zone, Vivid writer, Pierre Rollin, Bullshit, EHF, Misnomer, Karl Johnson, Bloody tears, DIC, Coagulation, IFN
| 2     | 13 Gorillas | 17 | 97-100  | England, EBOV, 1976, Laboratory, Porton Down, Geoffrey Platt, Not contagious, Very infectious, Fungal growth, Immune failure, Survived, Russian, Died
| 2     | 13 Gorillas | 18 | 100-111 | Kelly Warfield, Virologist, Nancy Jaax, USAMRIID, VLPs, 26, PhD, June 2002, Mouse, 11 Feb 2004, Light graze, The Slammer, Mordant
| 2     | 13 Gorillas | 19 | 111-113 | --
| 2     | 13 Gorillas | 20 | 114-117 | --
| 2     | 13 Gorillas | 21 | 117-122 | Peter Walsh, Wave theory, Reservoir hosts, Leslie Real, Distances, Eric Leroy, Independent introductions, Apes
| 2     | 13 Gorillas | 22 | 122-124 | Catherine Atsangandako, Prosper, Estelle
| 3     | Somewhere   | 23 | 127-129 | Malaria, Ronald Ross, 1874, West India, IMS, Madras, Alphonse Laveran, Protists, Life cycle, Nobel, 1902, Applied mathematics
| 3     | Somewhere   | 24 | 129-135 | CCS, Measles, 500000, David Bernoulli, 1760, Lifespan, 79, Life expectancy, John Snow, Broad St., 1854, Louis Pasteur, Robert Koch, Joseph Lister, WH Hamer, MAP, John Brownlee
| 3     | Somewhere   | 25 | 135-141 | P falciparum, Zoonotic, 4 malaria, Life cycle, Human adaptation, Polyphyletic, Chimpanzee malaria, Stephen Rich, Sabrina Krief, Ananias Escalante, Bonobo, Kinshasha, Weimin Liu, Beatrice Hahn, Poop, Western gorillas
| 3     | Somewhere   | 26 | 141-144 | Mathematics, Theory of epidemics, W.O. Kermack, A.G. McKendrick, Caustic alkali, 26 y/o, 3 factors, SIR modeling, 4th factor, Threshold density
| 3     | Somewhere   | 27 | 145-148 | Disease theory, George MacDonald, Ceylon, 1930s, Malaria epidemic, WHO, DDT, Basic reproduction rate, R-naught
| 3     | Somewhere   | 28 | 148-153 | Ananias Escalante, CDC, Robert Knowles, Das Gupta, *P. knowlesi*, Mihai Ciuca, Julius Wagner-Juaregg, Syphilis, Malaria, 1910s, 170 passages, 1937, Zoonosis, Malaysia, BW, American, Balbir Sign, Janet Cox-Singh
| 3     | Somewhere   | 29 | 153-156 | Kuching, *P. malariae*, Kapit, Filter paper, PCR test, *P. knowlesi*
| 3     | Somewhere   | 30 | 156-162 | UMS, Sarawak, *P. knowlesi*, Macaques, Filter paper technique, Blood spots, Nested PCR, 120 cases, The Lancet, Rejection, Death, Host switch
| 3     | Somewhere   | 31 | 162-164 | Asexual phase, Generalist, *P. knowlesi*, Pandemic potential
| 4     | Rat Farm    | 32 | 167-169 | 2003, SARS, Hong Kong, Carlo Urbani
| 4     | Rat Farm    | 33 | 169-175 | 16 Nov 2002, Foshan, Atypical pneumonia, Zhuo Zuofeng, Superspreader, Poison king, Liu Jianlun, 21 Feb 2003, Metropole
| 4     | Rat Farm    | 34 | 175-181 | Esther Mok, Metropole, Singapore, Brenda Ang, Leo Yee Sin, Tan Tock Seng, Hospital
| 4     | Rat Farm    | 35 | 182-187 | H5N1, History, Hong Kong, 1997, Chlamydia hypothesis, Malik Peiris, Publication, VERO, Leo Poon, Guan Yi, Civet cat
| 4     | Rat Farm    | 36 | 187-192 | Karl Taro Greenfeld, China syndrome, Era of Wild Flavor, Zoological bedlam, 2004, Another case

## Additional notes

- **Chapter 2:21**. On the surface, it looks like Eric Leroy and Peter Walsh had opposing theories for the introduction of EBOV in Africa. In actuality, it was not opposing views, rather different ways to look at the spread of EBOV. Leroy proposed multiple independent introductions of EBOV into primates. On the other hand, Walsh proposed the spread of EBOV in reservoir hosts took place in "wave", with the root EBOV strain could have been traced back to Yambuku 1976. See this publication: [Recent Common Ancestry of Ebola Zaire Virus Found in a Bat Reservoir]( https://doi.org/10.1371/journal.ppat.0020090)
- **Chapter 3**. The actual chapter title is too long, "Everything Comes From Somewhere". I shortened it to "Somewhere".
- **Chapter 3:24**. Lots of historical information in this sub-chapter, partically on David Bernoulli (applied mathematics on disease dynamics, 1760, smallpox, and increased life expectancy of smallpox were to be eradicated). Then, John Snow and his map of the cholera outbreak. John Brownlee proposed a different theory of diseases coming and go: the germs becoming *weakened* over time (i.e. loss of infectivity). However, Ross said "well, that is not quite possible". By the way, CCS means "critical community size", while MAP means "mass action principle".
- **Chapter 3**. It seems like the mathematical modeling theory of infectious disease came from malarial outbreak study.
- **Chapter 3:28**. Julius Wagner-Juaregg and Mihai Ciuca inoculated advanced-stage syphilis patients with malaria (*P. vivax* and *P. knowlesi*) as a mode of treatment. Horrifying, I know. They kept passaging their *P. knowlesi* strain in human, making them more virulent, *too virulent* for comfort eventually. Why it worked? Malaria increased the body temperature to a point it would cook *T. pallidum*, the syphilis-causing bacterium.
